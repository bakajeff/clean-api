import { Collection } from "mongodb";
import request from "supertest";
import { MongoHelper } from "../../src/infra/database/mongodb/mongo-helper";
import { app } from "../../src/infra/http/express/app";

describe("express /api", () => {
	let contactModel: Collection;

	beforeAll(async () => {
		await MongoHelper.connect("mongodb://localhost:27017");
	});

	beforeEach(async () => {
		contactModel = MongoHelper.getCollection("contacts");
		await contactModel.deleteMany();
	});

	afterEach(async () => {
		contactModel = MongoHelper.getCollection("contacts");
		await contactModel.deleteMany();
	});

	afterAll(async () => {
		await MongoHelper.disconnect();
	});

	describe("POST /contacts", () => {
		it("should create a contact", async () => {
			const inputData = {
				firstName: "John",
				lastName: "Smith",
				email: "john_smith@mail.com",
			};

			const response = await request(app).post("/api/contacts").send(inputData);

			expect(response.status).toBe(201);
		});
	});
});
