import { Collection } from "mongodb";
import request from "supertest";
import { MongoHelper } from "../../src/infra/database/mongodb/mongo-helper";
import { app } from "../../src/infra/http/express/app";

describe("express /api", () => {
	let contactModel: Collection;

	beforeAll(async () => {
		await MongoHelper.connect("mongodb://localhost:27017");
	});

	beforeEach(async () => {
		contactModel = MongoHelper.getCollection("contacts");
		await contactModel.deleteMany();
	});

	afterAll(async () => {
		await MongoHelper.disconnect();
	});

	describe("GET /contacts", () => {
		it("should return data", async () => {
			const inputData = {
				firstName: "John",
				lastName: "Smith",
				email: "john_smith@mail.com",
			};

			await contactModel.insertOne(inputData);

			const response = await request(app).get("/api/contacts");

			expect(response.status).toBe(200);
			expect(response.body).toHaveLength(1);
			expect(response.body).toEqual(
				expect.arrayContaining([
					expect.objectContaining({ firstName: "John" }),
					expect.objectContaining({ lastName: "Smith" }),
					expect.objectContaining({ email: "john_smith@mail.com" }),
				]),
			);
		});
	});
});
