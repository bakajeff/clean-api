export function serverError(error: Error) {
	return {
		statusCode: 500,
		body: Error("Internal Server Error"),
	};
}

export function created() {
	return {
		statusCode: 201,
		body: null,
	};
}

export function ok(body: any) {
	return {
		statusCode: 200,
		body,
	};
}
