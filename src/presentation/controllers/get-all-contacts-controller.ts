import { GetAllContactsUseCase } from "../../domain/interfaces/use-cases/contact/get-all-contacts-use-case";
import { serverError, ok } from "../helpers/http-helper";
import { Controller } from "../interfaces/controller";
import { HttpResponse } from "../interfaces/http-response";

export class GetAllContactsController implements Controller {
	constructor(private readonly getAllContactsUseCase: GetAllContactsUseCase) {}

	async handle(request: any): Promise<HttpResponse> {
		try {
			const contacts = await this.getAllContactsUseCase.execute();

			return ok(contacts);
		} catch (error) {
			return serverError(error as Error);
		}
	}
}
