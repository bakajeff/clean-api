import { Contact } from "../../domain/entities/contact";
import { CreateContactUseCase } from "../../domain/interfaces/use-cases/contact/create-contact-use-case";
import { CreateContactController } from "./create-contact-controller";

class CreateContactUseCaseSpy implements CreateContactUseCase {
	async execute(contact: Contact): Promise<boolean> {
		throw Error("Method not implemented");
	}
}

function makeFakeRequest() {
	return {
		firstName: "any_first_name",
		lastName: "any_last_name",
		email: "any@mail.com",
	};
}

const makeSut = () => {
	const createContactUseCase = new CreateContactUseCaseSpy();
	const sut = new CreateContactController(createContactUseCase);

	const createContactUseCaseSpy = jest.spyOn(createContactUseCase, "execute");

	return {
		createContactUseCaseSpy,
		sut,
	};
};

describe("Create Contract Controller", () => {
	it("should call CreateContactUseCase with correct values", async () => {
		const { sut, createContactUseCaseSpy } = makeSut();

		await sut.handle(makeFakeRequest());

		expect(createContactUseCaseSpy).toHaveBeenCalledWith(makeFakeRequest());
	});

	it("should return 500 if CreateContactController throws", async () => {
		const { sut, createContactUseCaseSpy } = makeSut();
		createContactUseCaseSpy.mockImplementationOnce(() =>
			Promise.reject(Error()),
		);

		const httpResponse = await sut.handle(makeFakeRequest());

		expect(httpResponse.statusCode).toBe(500);
	});

	it("should return 201 on success", async () => {
		const { sut, createContactUseCaseSpy } = makeSut();
		createContactUseCaseSpy.mockImplementationOnce(() => Promise.resolve(true));

		const httpResponse = await sut.handle(makeFakeRequest());

		expect(httpResponse.statusCode).toBe(201);
	});
});
