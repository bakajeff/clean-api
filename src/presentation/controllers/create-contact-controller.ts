import { Contact } from "../../domain/entities/contact";
import { CreateContactUseCase } from "../../domain/interfaces/use-cases/contact/create-contact-use-case";
import { created, serverError } from "../helpers/http-helper";
import { Controller } from "../interfaces/controller";
import { HttpResponse } from "../interfaces/http-response";

type Request = Contact;

export class CreateContactController implements Controller {
	constructor(private readonly createContactUseCase: CreateContactUseCase) {}

	async handle(request: Request): Promise<HttpResponse> {
		try {
			const { email, lastName, firstName } = request;

			await this.createContactUseCase.execute({
				email,
				lastName,
				firstName,
			});

			return created();
		} catch (error) {
			return serverError(error as Error);
		}
	}
}
