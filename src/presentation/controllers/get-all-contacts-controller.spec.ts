import { Contact } from "../../domain/entities/contact";
import { GetAllContactsUseCase } from "../../domain/interfaces/use-cases/contact/get-all-contacts-use-case";
import { GetAllContactsController } from "./get-all-contacts-controller";

class GetAllContactsUseCaseSpy implements GetAllContactsUseCase {
	execute(): Promise<Contact[]> {
		throw new Error("Method not implemented.");
	}
}

describe("Get All Contacts Controller", () => {
	it("should call GetAllContactsUseCase once", async () => {
		const getAllContactsUseCase = new GetAllContactsUseCaseSpy();
		const getAllContactsUseCaseSpy = jest
			.spyOn(getAllContactsUseCase, "execute")
			.mockImplementation(() => Promise.resolve([]));
		const sut = new GetAllContactsController(getAllContactsUseCase);

		await sut.handle({});

		expect(getAllContactsUseCaseSpy).toHaveBeenCalled();
	});

	it("should return 500 if GetAllContactsUseCase throws", async () => {
		const getAllContactsUseCase = new GetAllContactsUseCaseSpy();
		jest
			.spyOn(getAllContactsUseCase, "execute")
			.mockImplementation(() => Promise.reject(Error()));
		const sut = new GetAllContactsController(getAllContactsUseCase);

		const httpResponse = await sut.handle({});

		expect(httpResponse.statusCode).toBe(500);
	});

	it("should return 200 on success", async () => {
		const getAllContactsUseCase = new GetAllContactsUseCaseSpy();
		jest
			.spyOn(getAllContactsUseCase, "execute")
			.mockImplementation(() => Promise.resolve([]));
		const sut = new GetAllContactsController(getAllContactsUseCase);

		const httpResponse = await sut.handle({});

		expect(httpResponse.statusCode).toBe(200);
		expect(httpResponse.body).toBeTruthy();
	});
});
