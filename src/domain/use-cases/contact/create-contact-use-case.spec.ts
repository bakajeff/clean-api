import { Contact } from "../../entities/contact";
import { ContactRepository } from "../../interfaces/repositories/contact-repository";
import { CreateContact } from "./create-contact-use-case";

class ContactRepositorySpy implements ContactRepository {
	async getAll(): Promise<Contact[]> {
		throw new Error("Method not implemented.");
	}
	async createContact(): Promise<boolean> {
		throw Error("Method not implemented");
	}
}

const makeFakeContact = (): Contact => {
	return {
		email: "any@mail.com",
		lastName: "any_last_name",
		firstName: "any_first_name",
	};
};

describe("Create Contact Use Case", () => {
	it("should call ContactRepository createContact with correct values", async () => {
		const contactRepository = new ContactRepositorySpy();
		const contactRepositorySpy = jest
			.spyOn(contactRepository, "createContact")
			.mockImplementation(() => Promise.resolve(true));
		const sut = new CreateContact(contactRepository);

		await sut.execute(makeFakeContact());

		expect(contactRepositorySpy).toHaveBeenCalledWith(makeFakeContact());
	});

	it("should create a contact", async () => {
		const contactRepository = new ContactRepositorySpy();
		jest
			.spyOn(contactRepository, "createContact")
			.mockImplementation(() => Promise.resolve(true));
		const sut = new CreateContact(contactRepository);

		const result = await sut.execute(makeFakeContact());

		expect(result).toBe(true);
	});
});
