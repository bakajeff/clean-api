import { Contact } from "../../entities/contact";
import { ContactRepository } from "../../interfaces/repositories/contact-repository";
import { GetAllContacts } from "./get-all-contacts-use-case";

class ContactRepositorySpy implements ContactRepository {
	async getAll(): Promise<Contact[]> {
		throw new Error("Method not implemented.");
	}

	async createContact(): Promise<boolean> {
		throw Error("Method not implemented");
	}
}

const makeContact = (): Contact => {
	return {
		id: "any_id",
		email: "any@mail.com",
		lastName: "any_last_name",
		firstName: "any_first_name",
	};
};

describe("Create Contact Use Case", () => {
	it("should call ContactRepository getAll once", async () => {
		const contactRepository = new ContactRepositorySpy();
		const contactRepositorySpy = jest
			.spyOn(contactRepository, "getAll")
			.mockImplementation(() => Promise.resolve([]));
		const sut = new GetAllContacts(contactRepository);

		await sut.execute();

		expect(contactRepositorySpy).toHaveBeenCalled();
	});

	it("should return data", async () => {
		const contactRepository = new ContactRepositorySpy();
		jest
			.spyOn(contactRepository, "getAll")
			.mockImplementation(() => Promise.resolve([makeContact()]));
		const sut = new GetAllContacts(contactRepository);

		const result = await sut.execute();

		expect(result).toBeTruthy();
		expect(result.length).toBe(1);
	});
});
