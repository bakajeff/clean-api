import { Contact } from "../../entities/contact";
import { ContactRepository } from "../../interfaces/repositories/contact-repository";
import { GetAllContactsUseCase } from "../../interfaces/use-cases/contact/get-all-contacts-use-case";

export class GetAllContacts implements GetAllContactsUseCase {
	constructor(private readonly contactRepository: ContactRepository) {}
	async execute(): Promise<Contact[]> {
		const contacts = await this.contactRepository.getAll();

		return contacts;
	}
}
