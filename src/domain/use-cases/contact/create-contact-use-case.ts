import { Contact } from "../../entities/contact";
import { ContactRepository } from "../../interfaces/repositories/contact-repository";
import { CreateContactUseCase } from "../../interfaces/use-cases/contact/create-contact-use-case";

export class CreateContact implements CreateContactUseCase {
	constructor(private readonly contactRepository: ContactRepository) {}

	async execute(contact: Contact) {
		const result = await this.contactRepository.createContact(contact);
		return result;
	}
}
