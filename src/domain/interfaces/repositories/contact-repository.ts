import { Contact } from "../../entities/contact";

export interface ContactRepository {
	getAll(): Promise<Contact[]>;
	createContact(contact: Contact): Promise<boolean>;
}
