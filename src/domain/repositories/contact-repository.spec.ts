import { ContactDataSource } from "../../data/interfaces/data-sources/contact-data-source";
import { Contact } from "../entities/contact";
import { ContactRepositoryImpl } from "./contact-repository";

const makeFakeContact = (): Contact => {
	return {
		email: "any@mail.com",
		lastName: "any_last_name",
		firstName: "any_first_name",
	};
};

const makeContact = (): Contact => {
	return {
		id: "any_id",
		email: "any@mail.com",
		lastName: "any_last_name",
		firstName: "any_first_name",
	};
};

class ContactDataSourceSpy implements ContactDataSource {
	getAll(): Promise<Contact[]> {
		throw new Error("Method not implemented.");
	}
	create(contact: Contact): Promise<boolean> {
		throw new Error("Method not implemented.");
	}
}

describe("Contact Repository", () => {
	describe("getAll", () => {
		it("should return data", async () => {
			const contactDataSource = new ContactDataSourceSpy();
			jest
				.spyOn(contactDataSource, "getAll")
				.mockImplementation(() => Promise.resolve([makeContact()]));
			const contactRepository = new ContactRepositoryImpl(contactDataSource);

			const result = await contactRepository.getAll();

			expect(result).toBeTruthy();
			expect(result.length).toBe(1);
		});
	});

	describe("createContact", () => {
		it.todo("should call ContactDataSource with correct values");

		it("should return true", async () => {
			const contactDataSource = new ContactDataSourceSpy();
			jest
				.spyOn(contactDataSource, "create")
				.mockImplementation(() => Promise.resolve(true));
			const contactRepository = new ContactRepositoryImpl(contactDataSource);

			const result = await contactRepository.createContact(makeFakeContact());

			expect(result).toBe(true);
		});
	});
});
