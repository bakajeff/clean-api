import { ContactDataSource } from "../../data/interfaces/data-sources/contact-data-source";
import { Contact } from "../entities/contact";
import { ContactRepository } from "../interfaces/repositories/contact-repository";

export class ContactRepositoryImpl implements ContactRepository {
	constructor(private readonly contactDataSource: ContactDataSource) {}
	async getAll(): Promise<Contact[]> {
		const contacts = await this.contactDataSource.getAll();
		return contacts;
	}

	async createContact(contact: Contact): Promise<boolean> {
		const result = await this.contactDataSource.create(contact);
		return result;
	}
}
