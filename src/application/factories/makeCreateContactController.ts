import { MongoDbContactDataSource } from "../../data/data-sources/mongodb/mongodb-contact-data-source";
import { ContactRepositoryImpl } from "../../domain/repositories/contact-repository";
import { CreateContact } from "../../domain/use-cases/contact/create-contact-use-case";
import { MongoRepository } from "../../infra/database/mongodb/mongo-repository";
import { CreateContactController } from "../../presentation/controllers/create-contact-controller";
import { Controller } from "../../presentation/interfaces/controller";

export function makeCreateContactController(): Controller {
	const controller = new CreateContactController(
		new CreateContact(
			new ContactRepositoryImpl(
				new MongoDbContactDataSource(new MongoRepository()),
			),
		),
	);

	return controller;
}
