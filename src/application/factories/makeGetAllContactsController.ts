import { MongoDbContactDataSource } from "../../data/data-sources/mongodb/mongodb-contact-data-source";
import { ContactRepositoryImpl } from "../../domain/repositories/contact-repository";
import { GetAllContacts } from "../../domain/use-cases/contact/get-all-contacts-use-case";
import { MongoRepository } from "../../infra/database/mongodb/mongo-repository";
import { GetAllContactsController } from "../../presentation/controllers/get-all-contacts-controller";
import { Controller } from "../../presentation/interfaces/controller";

export function makeGetAllContactsController(): Controller {
	const controller = new GetAllContactsController(
		new GetAllContacts(
			new ContactRepositoryImpl(
				new MongoDbContactDataSource(new MongoRepository()),
			),
		),
	);

	return controller;
}
