import * as dotenv from "dotenv";
dotenv.config();

export default {
	mongoUri: process.env.MONGO_URI || "mongodb://localhost:27017/clean-node-api",
	restPort: process.env.REST_PORT || 3000,
	graphqlPort: process.env.GRAPHQL_PORT || 4000,
};
