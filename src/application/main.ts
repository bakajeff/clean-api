import { MongoHelper } from "../infra/database/mongodb/mongo-helper";
import { startStandaloneServer } from "@apollo/server/standalone";

import { app } from "../infra/http/express/app";
import { apolloApp } from "../infra/graphql/app";

import env from "./config/env";

MongoHelper.connect(env.mongoUri)
	.then(async () => {
		app.listen(env.restPort, () =>
			console.log(`App running on port ${env.restPort}`),
		);
		const { url } = await startStandaloneServer(apolloApp, {
			listen: { port: 4000 },
		});

		console.log(`🚀  Server ready at: ${url}`);
	})
	.catch(console.error);
