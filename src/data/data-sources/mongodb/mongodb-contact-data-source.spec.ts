import { Contact } from "../../../domain/entities/contact";
import { DatabaseWrapper } from "../../interfaces/data-sources/database-wrapper";
import { MongoDbContactDataSource } from "./mongodb-contact-data-source";

const makeFakeContact = (): Contact => {
	return {
		email: "any@mail.com",
		lastName: "any_last_name",
		firstName: "any_first_name",
	};
};

describe("MongoDB Contact Data Source", () => {
	let database: DatabaseWrapper;

	beforeAll(async () => {
		database = {
			find: jest.fn(),
			insertOne: jest.fn(),
		};
	});

	it("find", async () => {
		const sut = new MongoDbContactDataSource(database);
		jest.spyOn(database, "find").mockImplementation(() => Promise.resolve([]));

		const result = await sut.getAll();

		expect(database.find).toHaveBeenCalled();
		expect(result).toBeTruthy();
	});

	it("create", async () => {
		const sut = new MongoDbContactDataSource(database);
		jest
			.spyOn(database, "insertOne")
			.mockImplementation(() => Promise.resolve(true));

		const result = await sut.create(makeFakeContact());

		expect(database.insertOne).toHaveBeenCalledWith(makeFakeContact());
		expect(result).toBe(true);
	});
});
