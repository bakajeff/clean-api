import { Contact } from "../../../domain/entities/contact";
import { ContactDataSource } from "../../interfaces/data-sources/contact-data-source";
import { DatabaseWrapper } from "../../interfaces/data-sources/database-wrapper";

export class MongoDbContactDataSource implements ContactDataSource {
	constructor(private readonly database: DatabaseWrapper) {}
	async getAll(): Promise<Contact[]> {
		const result = await this.database.find({});

		return result.map((item) => ({
			id: item._id.toString(),
			lastName: item.lastName,
			firstName: item.firstName,
			email: item.email,
		}));
	}

	async create(contact: Contact): Promise<boolean> {
		const result = await this.database.insertOne(contact);

		return result;
	}
}
