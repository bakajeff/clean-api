import { Contact } from "../../../domain/entities/contact";

export interface ContactDataSource {
	getAll(): Promise<Contact[]>;
	create(contact: Contact): Promise<boolean>;
}
