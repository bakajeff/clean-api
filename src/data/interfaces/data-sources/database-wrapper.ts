export interface DatabaseWrapper {
	find(query: object): Promise<any[]>;
	insertOne(data: any): Promise<any>;
}
