import { Controller } from "../../../presentation/interfaces/controller";

export const adaptResolver = async (
	controller: Controller,
	args?: any,
	context?: any,
): Promise<any> => {
	const request = {
		...(args || {}),
	};

	const httpResponse = await controller.handle(request);

	switch (httpResponse.statusCode) {
		case 200:
			return httpResponse.body;
		case 201:
			break;
		default:
			throw new Error(httpResponse.body.message);
	}
};
