export default `#graphql
type Contact {
	firstName: String!
	lastName: String!
	email: String!
}

type Query {
	contacts: [Contact]
}
`;
