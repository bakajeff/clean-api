import { makeGetAllContactsController } from "../../../application/factories/makeGetAllContactsController";
import { adaptResolver } from "../adapters/resolver-adapter";

export default {
	Query: {
		contacts: async (parent: any, args: any, context: any) =>
			adaptResolver(makeGetAllContactsController(), args, context),
	},
};
