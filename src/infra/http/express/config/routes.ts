import { Router, Express } from "express";
import contactsRoutes from "../routes/contacts-routes";

export default function (app: Express): void {
	const router = Router();
	app.use("/api", router);

	contactsRoutes(router);
}
