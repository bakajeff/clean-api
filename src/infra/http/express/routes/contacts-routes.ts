import { Router } from "express";
import { makeCreateContactController } from "../../../../application/factories/makeCreateContactController";
import { makeGetAllContactsController } from "../../../../application/factories/makeGetAllContactsController";
import { adaptRoute } from "../adapter/router-adapter";

export default function (router: Router): void {
	router.get("/contacts", adaptRoute(makeGetAllContactsController()));
	router.post("/contacts", adaptRoute(makeCreateContactController()));
}
