import express from "express";

import setupRoutes from "./config/routes";

const app = express();
app.use(express.json());
setupRoutes(app);

export { app };
