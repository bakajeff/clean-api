import { Request, Response } from "express";

import { Controller } from "../../../../presentation/interfaces/controller";

export function adaptRoute(controller: Controller) {
	return async (request: Request, response: Response) => {
		const httpRequest = {
			...(request.body || {}),
			...(request.params || {}),
		};
		const httpResponse = await controller.handle(httpRequest);

		if (httpResponse.statusCode >= 200 && httpResponse.statusCode <= 299) {
			return response.status(httpResponse.statusCode).json(httpResponse.body);
		} else {
			return response.status(httpRequest.statusCode).json({
				error: httpRequest.body.message,
			});
		}
	};
}
