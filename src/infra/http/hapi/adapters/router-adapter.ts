import { Request, ResponseToolkit } from "@hapi/hapi";

import { Controller } from "../../../../presentation/interfaces/controller";

export function adaptRoute(controller: Controller) {
	return async (request: Request, h: ResponseToolkit) => {
		const httpRequest = {
			...((request.payload as object) || {}),
			...(request.params || {}),
		};
		const httpResponse = await controller.handle(httpRequest);

		if (httpResponse.statusCode >= 200 && httpResponse.statusCode <= 299) {
			return h.response(httpResponse.body).code(httpResponse.statusCode);
		} else {
			return h
				.response({
					error: httpResponse.body.message,
				})
				.code(httpResponse.statusCode);
		}
	};
}
