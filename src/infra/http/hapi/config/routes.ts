import { Server } from "@hapi/hapi";
import contactRoutes from "../routes/contact-routes";

export default function (app: Server) {
	app.route(contactRoutes());
}
