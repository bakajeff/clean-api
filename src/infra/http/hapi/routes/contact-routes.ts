import { makeCreateContactController } from "../../../../application/factories/makeCreateContactController";
import { makeGetAllContactsController } from "../../../../application/factories/makeGetAllContactsController";
import { adaptRoute } from "../adapters/router-adapter";

export default function () {
	return [
		{
			method: "GET",
			path: "/api/contacts",
			handler: adaptRoute(makeGetAllContactsController()),
		},
		{
			method: "POST",
			path: "/api/contacts",
			handler: adaptRoute(makeCreateContactController()),
		},
	];
}
