import Hapi from "@hapi/hapi";

import setupRoutes from "./config/routes";

const app = Hapi.server({
	port: 3333,
	host: "localhost",
});

setupRoutes(app);

export { app };
