import { DatabaseWrapper } from "../../../data/interfaces/data-sources/database-wrapper";
import { Contact } from "../../../domain/entities/contact";
import { MongoHelper } from "./mongo-helper";

export class MongoRepository implements DatabaseWrapper {
	async find(query: object): Promise<any[]> {
		return await MongoHelper.getCollection("contacts").find(query).toArray();
	}

	async insertOne(data: any): Promise<any> {
		return await MongoHelper.getCollection("contacts").insertOne(data);
	}
}
